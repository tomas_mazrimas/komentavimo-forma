<?php

//add_comment.php

$connect = new PDO('mysql:host=localhost;dbname=dbaze', 'root', 'root');

$error = '';
$comment_name = '';
$comment_content = '';

if(empty($_POST["comment_name"]))
{
 $error .= '<p class="text-danger">Name is required</p>';
}
else
{
 $comment_name = $_POST["comment_name"];
}

if(empty($_POST["comment_content"]))
{
 $error .= '<p class="text-danger">Comment is required</p>';
}
else
{
 $comment_content = $_POST["comment_content"];
}

if(empty($_POST["comment_email"]))
{
 $error .= '<p class="text-danger">Email is required</p>';
}
else
{
 $comment_email = $_POST["comment_email"];
}
if($error == '')
{
 $query = "
 INSERT INTO tbl_comment 
 (parent_comment_id, comment, comment_sender_name, email) 
 VALUES (:parent_comment_id, :comment, :comment_sender_name, :email)
 ";
 $statement = $connect->prepare($query);
 $statement->execute(
  array(
   ':parent_comment_id' => $_POST["comment_id"],
   ':comment'    => $comment_content,
   ':comment_sender_name' => $comment_name,
    ':email' => $comment_email
  )
 );
 $error = '<label class="text-success"></label>';
}

$data = array(
 'error'  => $error
);

echo json_encode($data);

?>